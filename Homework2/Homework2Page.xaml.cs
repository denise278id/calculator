﻿using System;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework2
{
    public partial class Homework2Page : ContentPage
    {
        /* jjb
    The danger of comments is that you have to remember to update them if/when you change the
    method in the future. Most developers forget to do this, so many developers choose to never add comments
    unless there is something SUPER weird, hacky, or tricky that isn't clear from reading the code.
    Otherwise, they try to write all code as "self documenting", by which I mean giving everything super clear
    names so that code doesn't need comments.

    Using the name "attendeeCounter" for input does not seem to really make sense here. Consider using a name that really
    describes what the variable is for, so you do not need a comment to explain it. This will pay off in huge dividends as
    you write more and more complex code, whether mobile or otherwise.
    
    "Code never lies... Comments sometimes do." - Ron Jeffries, https://en.wikipedia.org/wiki/Ron_Jeffries
 */  

        //attendee counter is our input
        string attendeeCounter = string.Empty;
        string operand1 = string.Empty;
        string operand2 = string.Empty;
        char operation;
        Double result = 0.0;


        public Homework2Page()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Homework2Page)}:  constructor");

            InitializeComponent();

            //  Numbers();
        }

        private void Zero_Clicked(object sender, System.EventArgs e)
        {
            this.AttendeCounterLabel.Text = "";
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Zero_Clicked)}:  Adding number {attendeeCounter = attendeeCounter + 0}");
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
            this.AttendeCounterLabel.Text += attendeeCounter;
            AttendeCounterLabel.Text = $"{attendeeCounter} ";

        }

        private void One_Clicked(object sender, System.EventArgs e)
        {
            this.AttendeCounterLabel.Text = "";
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(One_Clicked)}:  Adding  number {attendeeCounter = attendeeCounter + 1}");
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
            this.AttendeCounterLabel.Text += attendeeCounter;
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
        }
        private void Two_Clicked(object sender, System.EventArgs e)
        {
            this.AttendeCounterLabel.Text = "";
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Two_Clicked)}:  Adding  number {attendeeCounter = attendeeCounter + 2}");
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
            this.AttendeCounterLabel.Text += attendeeCounter;
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
        }

        private void Three_Clicked(object sender, System.EventArgs e)
        {
            this.AttendeCounterLabel.Text = "";
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Three_Clicked)}:  Adding  number {attendeeCounter = attendeeCounter + 3}");
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
            this.AttendeCounterLabel.Text += attendeeCounter;
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
        }
        private void Four_Clicked(object sender, System.EventArgs e)
        {
            this.AttendeCounterLabel.Text = "";
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Four_Clicked)}:  Adding number {attendeeCounter = attendeeCounter + 4}");
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
            this.AttendeCounterLabel.Text += attendeeCounter;
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
        }
        private void Five_Clicked(object sender, System.EventArgs e)
        {
            this.AttendeCounterLabel.Text = "";
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Five_Clicked)}:  Adding number {attendeeCounter = attendeeCounter + 5}");
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
            this.AttendeCounterLabel.Text += attendeeCounter;
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
        }

        private void Six_Clicked(object sender, System.EventArgs e)
        {
            this.AttendeCounterLabel.Text = "";
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Six_Clicked)}:  Adding number {attendeeCounter = attendeeCounter + 6}");
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
            this.AttendeCounterLabel.Text += attendeeCounter;
            AttendeCounterLabel.Text = $"{attendeeCounter} ";

        }


        private void Seven_Clicked(object sender, System.EventArgs e)
        {
            this.AttendeCounterLabel.Text = "";
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Seven_Clicked)}:  Adding number {attendeeCounter = attendeeCounter + 7}");
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
            this.AttendeCounterLabel.Text += attendeeCounter;
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
        }



        private void Eight_Clicked(object sender, System.EventArgs e)
        {
            this.AttendeCounterLabel.Text = "";
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Eight_Clicked)}:  Adding number {attendeeCounter = attendeeCounter + 8}");
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
            this.AttendeCounterLabel.Text += attendeeCounter;
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
        }

        private void Nine_Clicked(object sender, System.EventArgs e)
        {
            this.AttendeCounterLabel.Text = "";
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Nine_Clicked)}:  Adding number {attendeeCounter = attendeeCounter + 9}");
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
            this.AttendeCounterLabel.Text += attendeeCounter;
            AttendeCounterLabel.Text = $"{attendeeCounter} ";
        }

        private void Clear_Clicked(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Clear_Clicked)}:  Adding number {attendeeCounter = attendeeCounter + 1}");
            AttendeCounterLabel.Text = "";
            this.attendeeCounter = string.Empty;
            this.operand1 = string.Empty;
            this.operand2 = string.Empty;
            AttendeCounterLabel.Text = $"0";
        }

        private void Add_Clicked(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Add_Clicked)}: make operan {operand1 = attendeeCounter }");
            operation = '+';
            attendeeCounter = string.Empty;

        }
        private void Subtract_Clicked(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Subtract_Clicked)}: make operan {operand1 = attendeeCounter }");
            operation = '-';
            attendeeCounter = string.Empty;

        }
        private void Multiplication_Clicked(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Multiplication_Clicked)}: make operan {operand1 = attendeeCounter }");
            operation = '*';
            attendeeCounter = string.Empty;

        }
        private void Division_Clicked(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Division_Clicked)}: make operan {operand1 = attendeeCounter }");
            operation = '/';
            attendeeCounter = string.Empty;

        }
        private void Equal_Clicked(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Equal_Clicked)}: make operand {operand2 = attendeeCounter }");

            double num1, num2;
            double.TryParse(operand1, out num1);
            double.TryParse(operand2, out num2);
            if (operation == '+')
            {
                result = num1 + num2;
                AttendeCounterLabel.Text = result.ToString();


            }
            else if (operation == '-')
            {
                result = num1 - num2;
                AttendeCounterLabel.Text = result.ToString();

            }
            else if (operation == '*')
            {
                result = num1 * num2;
                AttendeCounterLabel.Text = result.ToString();

            }
            else if (operation == '/')
            {
                result = num1 / num2;
                AttendeCounterLabel.Text = result.ToString();

            }
           

        }



    }
}
